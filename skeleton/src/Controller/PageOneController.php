<?php
// src/Controller/testcontroller.php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Abstractcontroller;
use Symfony\Component\Routing\Annotation\Route;

class PageOneController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index()
    {
        return $this->render('index.html.twig');
    }
}
